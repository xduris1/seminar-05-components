package cz.muni.fi.pa165.currency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;


/**
 * Base implementation of {@link CurrencyConvertor}.
 *
 * @author petr.adamek@embedit.cz
 */
public class CurrencyConvertorImpl implements CurrencyConvertor {

    private final ExchangeRateTable exchangeRateTable;
    // uncomment and import classes from org.slf4j package
    private final Logger logger = LoggerFactory.getLogger(CurrencyConvertorImpl.class);

    public CurrencyConvertorImpl(ExchangeRateTable exchangeRateTable) {
        this.exchangeRateTable = exchangeRateTable;
    }

    @Override
    public BigDecimal convert(Currency sourceCurrency, Currency targetCurrency, BigDecimal sourceAmount) throws IllegalArgumentException, ExternalServiceFailureException {
        logger.trace("trace {},{},{}",sourceCurrency,targetCurrency,sourceAmount);

        if(sourceCurrency == null || targetCurrency == null || sourceAmount == null){
            throw new IllegalArgumentException("");
        }
        try{
            var rate = exchangeRateTable.getExchangeRate(sourceCurrency,targetCurrency);
            if(rate == null){
                logger.warn("warning");
                throw new UnknownExchangeRateException("");
            }
            return rate.multiply(sourceAmount).setScale(2, RoundingMode.HALF_EVEN);
        } catch (ExternalServiceFailureException e) {
            logger.error("error");
            throw new ExternalServiceFailureException("exception");
        }
    }
}
