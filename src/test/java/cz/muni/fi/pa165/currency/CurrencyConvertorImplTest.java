package cz.muni.fi.pa165.currency;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Currency;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CurrencyConvertorImplTest {

    Currency currencyEUR = Currency.getInstance("EUR");
    Currency currencyCZK = Currency.getInstance("CZK");

    @Mock
    ExchangeRateTable exchangeRateTable;
    CurrencyConvertor currencyConvertor;

    @BeforeEach
    public void setup() {
        currencyConvertor = new CurrencyConvertorImpl(exchangeRateTable);
    }


    @Test
    public void testConvert() throws ExternalServiceFailureException, IllegalAccessException {

        // Don't forget to test border values and proper rounding.
        when(exchangeRateTable.getExchangeRate(currencyEUR, currencyCZK)).thenReturn(new BigDecimal("25"));
        assertThat(currencyConvertor.convert(currencyEUR, currencyCZK, new BigDecimal("0"))).isEqualTo(new BigDecimal("0.00"));
        assertThat(currencyConvertor.convert(currencyEUR,currencyCZK,  new BigDecimal("100000000000000000000"))).isEqualTo(new BigDecimal("2500000000000000000000.00"));
    }

    @Test
    public void testConvertWithNullSourceCurrency() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> currencyConvertor.convert(null, currencyEUR, new BigDecimal("1")));
    }

    @Test
    public void testConvertWithNullTargetCurrency() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> currencyConvertor.convert( currencyEUR,null, new BigDecimal("1")));
    }

    @Test
    public void testConvertWithNullSourceAmount() {
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> currencyConvertor.convert( currencyEUR,currencyCZK,null));
    }

    @Test
    public void testConvertWithUnknownCurrency() {
        Currency currency = Currency.getInstance("USD");
        assertThatExceptionOfType(UnknownExchangeRateException.class).isThrownBy(() -> currencyConvertor.convert( currencyEUR,currency,new BigDecimal("1")));
    }

    @Test
    public void testConvertWithExternalServiceFailure() throws ExternalServiceFailureException {
        when(exchangeRateTable.getExchangeRate(currencyEUR,currencyCZK)).thenThrow(new ExternalServiceFailureException(""));
        assertThatExceptionOfType(ExternalServiceFailureException.class).isThrownBy(() -> currencyConvertor.convert( currencyEUR,currencyCZK,new BigDecimal("1")));

    }

}
