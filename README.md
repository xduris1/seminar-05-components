**Prerequisites:** Knowledge of
  - `JUnit` testing framework
  - `Mockito` mocking framework
  - `SLF4J` logging facade
  - `Logback` logging framework

## Seminar Tasks
**Task 01**\
Clone the project and open it in your IDE. Look at the interfaces `ExchangeRateTable` and
`CurrencyConvertor` in the package `cz.fi.muni.pa165.currency` and read their contract.

**Task 02**\
Open `pom.xml` and add Maven dependencies for testing: `mockito-core`, `mockito-junit-jupiter`, `assertj-core`.
JUnit dependency `junit-jupiter` is already present in the `pom.xml`

```xml
    <dependency>
        <!-- Rich and fluent assertions -->
        <groupId>org.assertj</groupId>
        <artifactId>assertj-core</artifactId>
        <version>${version.assertj}</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <!-- Mocking framework  -->
        <groupId>org.mockito</groupId>
        <artifactId>mockito-core</artifactId>
        <version>${version.mockito}</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <!-- Mockito's JUnit 5 (JUnit Jupiter) support library -->
        <groupId>org.mockito</groupId>
        <artifactId>mockito-junit-jupiter</artifactId>
        <version>${version.mockito}</version>
        <scope>test</scope>
    </dependency>
```
**Tip**: Reload Maven project in `Maven tool window` if IntelliJ IDEA does not recognize newly added dependencies.

**Notice** that
  - `dependency versions` are described as `properties`. Values of these properties are already defined in the `pom.xml`.
  - scope of the dependencies is set to `test`.

**Task 03**\
Implement `CurrencyConvertorImplTest::testConvert` method.\
Use [Mockito](http://site.mockito.org/) for creating mocks, 
[AssertJ](http://joel-costigliola.github.io/assertj/)
for assertions and do not forget to test border values and proper
rounding.\
Ask your teacher to check if the test is well written.

**Hint**: Read [Mockito tutorial](http://www.vogella.com/tutorials/Mockito/article.html) \
**Tip**: It is better to use `new BigDecimal("15.29")` than `new BigDecimal(15.29)`\
for creating `BigDecimal values`. Do you know why?

**Task 04**\
Implement all other test methods in `CurrencyConvertorImplTest`. 

**Task 05**\
Implement `CurrencyConvertorImpl::convert` method and try to execute tests.

**Task 06**\
Add `slf4j-api` to Maven dependencies
```xml
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${version.slf4j}</version>
    </dependency>
```
and modify `CurrencyConvertorImpl` class to log:
  - Each call of `convert()` method as a **trace**
  - Each `conversion failure` due `missing exchange rate for given currencies` as a **warning**
  - Each `conversion failure` due `ExternalServiceFailureException` as an **error**

Do not forget to log all useful context information.\
**Hint**: `SL4J` allows simple string interpolation, [see section with 'parametrized messages'](https://www.slf4j.org/faq.html#logging_performance).

**Task 07**\
Add `Logback` to Maven dependencies
```xml
    <dependency>
        <!-- Logging framework -->
        <groupId>ch.qos.logback</groupId>
        <artifactId>logback-classic</artifactId>
        <version>${version.logback}</version>
    </dependency>
```
Change logback configuration to log **trace** messages (see `logback.xml`) and run the tests to check if the logging works.
